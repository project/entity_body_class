# Entity body class

Provides a new "Body CSS class(es)" field to add body classes.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/entity_body_class).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/entity_body_class).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Configure permissions:
Home >> Administration >> People
(/admin/people/permissions/module/entity_body_class)

- Add field values:
Go to the entity form and add some body classes to the field.


## Maintainers

- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
